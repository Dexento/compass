package com.Dexento.me.compass;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerJoin implements Listener {

	public PlayerJoin(Main plugin) {
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		ItemStack comp = new ItemStack(Material.COMPASS);
		ItemMeta compMeta = comp.getItemMeta();
		compMeta.setDisplayName(ChatColor.YELLOW + "Quick Travel");
		comp.setItemMeta(compMeta);
		p.getInventory().clear();
		p.getInventory().setItem(0, comp);
	}
}
