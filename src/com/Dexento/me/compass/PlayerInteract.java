package com.Dexento.me.compass;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract implements Listener {

	public PlayerInteract(Main plugin) {
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		Action eAction = e.getAction();
		Material inHand = p.getItemInHand().getType();
		
		if (eAction != Action.PHYSICAL && eAction != Action.LEFT_CLICK_AIR && eAction != Action.LEFT_CLICK_BLOCK) {
			if (inHand == Material.COMPASS) {
				Main.warpMenu.open(p);
			}
		}
	}
}
